def matryoshka (n):
    if n == 1:
        print("Матрёшка")
    else:
        print("Верх матрёшки n=", n)
        matryoshka(n-1)
        print("Низ матрёшки n=", n)

matryoshka(4)
