def if_number_is_simple (x):

    divisor = 2
    while divisor < x:
        if x % divisor == 0:
            return False
        divisor += 1

    return True
