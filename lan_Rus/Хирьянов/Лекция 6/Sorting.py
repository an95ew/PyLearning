def insert_sort(sorting_list:list):
    """Сортировка массива вставкой"""
    N = len(sorting_list)
    for bypass in range(1, N):
        i = bypass
        while i>0 and sorting_list[i-1]>sorting_list[i]:
            sorting_list[i-1], sorting_list[i] = sorting_list[i], sorting_list[i-1]
            i -= 1

def select_sort(sorting_list:list):
    """Сортировка массива выбором"""
    N = len(sorting_list)
    for bypass in range(N-1):
        min_index = bypass
        for i in range(bypass+1, N):
            if sorting_list[min_index] > sorting_list[i]:
                min_index = i
        sorting_list[bypass], sorting_list[min_index] = sorting_list[min_index], sorting_list[bypass]

def bubble_sort(sorting_list:list):
    """Сортировка массива пузырьком"""
    N = len(sorting_list)
    for bypass in range(N, 0, -1):
        for i in range(bypass-1):
            if sorting_list[i]>sorting_list[i+1]:
                sorting_list[i], sorting_list[i+1] = sorting_list[i+1], sorting_list[i]


def sorting_function_test(testing_func):


    print("Testing:", testing_func.__doc__ )
    A = [3, 5, 1, 4, 2]
    A_sorted = list(range(1,6))
    testing_func(A)
    print("Case #1 test - OK!" if A == A_sorted else "Case #1 test - FAIL!")
    print(A)
    
    A = list(range(10,20))
    B = list(range(1,10))
    C = list(A+B)
    C_sorted = list(range(1, 20))
    testing_func(C)
    print("Case #2 test - OK!" if C == C_sorted else "Case #2 test - FAIL!")
    print(C)
    
    A = [1, 4, 1, 4, 2]
    A_sorted = [1, 1, 2, 4, 4]
    testing_func(A)
    print("Case #3 test - OK!" if A == A_sorted else "Case #3 test - FAIL!")
    print(A)


sorting_function_test(insert_sort)
sorting_function_test(select_sort)
sorting_function_test(bubble_sort)
