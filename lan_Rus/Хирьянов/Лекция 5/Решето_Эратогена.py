N = 100
A = [True] * N
A[0]=A[1]=False

for i in range (2, N):
    if A[i]:
        for m in range (2*i, N, i):
            A[m] = False

for i in range(N):
    print(i, "-", "простое" if A[i] else "составное")
