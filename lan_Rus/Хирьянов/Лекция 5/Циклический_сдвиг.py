N = 5
A = [1, 2, 3, 4, 5]

#ЦИКЛИЧЕСКИЙ СДВИГ ---ВЛЕВО---
print(A)
tmp = A[0]

for i in range (N-1):
    A[i] = A[i+1]
A[N-1] = tmp

print(A)


#ЦИКЛИЧЕСКИЙ СДВИГ ---ВПРАВО---
A = [1, 2, 3, 4, 5]
print(A)
tmp = A[N-1]

for i in range(N-2,-1,-1):
    A[i+1] = A[i]
A[0] = tmp
print(A)    
