import functools

def logger (func):
    @functools.wraps(func) # используется для того, чтобы оригинальное имя функции не изменялось в процессе декорирования
    def wrapp (*args, **kwargs):
        result = func (*args, **kwargs)
        with open ('log.txt', 'w') as file:
            file.write(str(result))

        return result
    
    return wrapp

@logger
def summator(num_list):
    return sum(num_list)

print('Summator: {}'.format(summator([1,2,3,4,5])))

with open('log.txt','r') as f:
    print('logger: {}'.format(f.read()))
    
print(summator.__name__)
