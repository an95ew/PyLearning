empty_set = set() # изменяемое множество
frozen = frozenset() # не изменяемое множество
number_set = {1, 2, 3, 3, 4, 5}


odd_set = set()
even_set = set()

for numbers in range(10):
    if numbers % 2:
        odd_set.add(numbers)
    else:
        even_set.add(numbers)

print(odd_set)
print(even_set)

union_set = odd_set.union(even_set)
print(union_set)

#intersection ( | )- пересечение  (показывает какие значения совпадают)

#difference ( - )- если n_set = n_set.difference(a_set), то показывает какие значения
#(n) не найдены в множестве (а)

#Для удаления элементов из множества используется метод .remove(значение)
