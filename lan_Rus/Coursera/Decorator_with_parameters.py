def logger(filename):
    def decorator(func):
        def wrapp(*args,**kwargs):
            result = func (*args, **kwargs)
            with open (filename, 'w') as f:
                f.write(str(result))
            return result
        return wrapp 
    return decorator



@logger('new_log.txt')
def summator (num_list):
    return sum(num_list)

print('Summator: {}'.format(summator([1,2,3,4,5,6,7])))

