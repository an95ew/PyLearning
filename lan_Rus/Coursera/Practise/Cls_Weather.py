import pprint
import requests
from dateutil.parser import parse
 
class OpenWeather:
    
    #Симулируем КЭШ-систему для хранения ранее запрашиваемых прогнозов погоды
    def __init__ (self):
        OpenWeather._city_cache = {}
    
    def get (self, city):
        if city in OpenWeather._city_cache:
            return OpenWeather._city_cache[city]
        url = f"https://api.openweathermap.org/data/2.5/forecast?q={city}&units=metric&appid=7b6b64b1d5e18264f25e1243941f517f"
        print("Sending HHTP-request...")
        data = requests.get(url).json()
        forecast_data = data["list"]
        forecast = []
        for day_data in range(len(forecast_data)):
            forecast.append({
                "date": parse(forecast_data[day_data]["dt_txt"]),
                "max_temp": forecast_data[day_data]["main"]["temp_max"],
                "min_temp": forecast_data[day_data]["main"]["temp_min"]
                })
        OpenWeather._city_cache[city] = forecast
        return forecast

class CityInfo:
    def __init__ (self, city, weather_forecast = None):
        self._city = city
        self._weather_forecast = weather_forecast or OpenWeather()
        
    def weather_forecast(self):
        return self._weather_forecast.get(self._city)
    
         
def _main():
    weather_forecast = OpenWeather()
    for i in range(3):
        city_info = CityInfo("Kiev", weather_forecast = weather_forecast)
        city_info.weather_forecast()
    #pprint.pprint(forecast)


if __name__ == "__main__":
    _main()