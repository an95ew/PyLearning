from tempfile import gettempdir
from os import path, remove
from json import loads, dumps
from argparse import ArgumentParser

STORAGE_PATH = path.join(gettempdir(), 'storage.data')
print(STORAGE_PATH)

def get_data():
    if not path.exists(STORAGE_PATH):
        return{}

    with open(STORAGE_PATH, 'r') as f:
        file_data = f.read()
        if file_data:
            return loads(file_data)

        return {}

def put(key, value):
    data = get_data()

    if key in data:
        data[key].append(value)
    else:
        data[key] = [value]

    with open(STORAGE_PATH, 'w') as f:
        f.write(dumps(data))

def get (key):
    data = get_data()

    return data.get(key)




if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--key', help='Key')
    parser.add_argument('--value', help='Value')
    parser.add_argument('--clear', action='store_true', help='Clear')

    args = parser.parse_args()

    if args.clear:
        remove(STORAGE_PATH)
    elif args.key and args.value:
        put(args.key, args.value)
    elif args.key:
        print(get(args.key))
    else:
        print('Wrong command')
    














    
