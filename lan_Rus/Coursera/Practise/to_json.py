from functools import wraps
from json import loads, dumps


def to_json (func):

    @wraps(func)
    def wrapper (*args,**kwargs):
        #data = str(func())
        #json_data = dumps(data)
        #return json_data
        return dumps(func(*args, **kwargs))
    
    return wrapper



@to_json
def get_data():
  return {
    'data': 42
  }
  
print(type(get_data()))  # вернёт '{"data": 42}'
print(get_data.__name__)
