class Robot:
    def __init__(self, power):
        self._power = power
    
    power = property()
    
    @power.getter
    def power(self):
        return self._power
    
    @power.setter
    def power(self, value):
        if value < 0:
            self._power = 0
        else:
            self._power = value
            
    @power.deleter
    def power(self):
        print("Make robot useless")
        del self._power
        
wall_e = Robot(100)
print(wall_e.power)

wall_e.power = -13
print(wall_e.power)


############################ 2nd variant #############################

class Robot_short:
    
    def __init__ (self, power):
        self._power = power
        
    @property
    def power(self):
        # здесь могут быть любые полезные вычисления
        return self._power
    
wall_o = Robot_short(90)
print(wall_o.power)