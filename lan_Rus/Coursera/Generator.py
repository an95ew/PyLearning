def even_range(start, end):
    current = start
    while current < end:
        yield current
        current += 2

for num in even_range(0,5):
    print(num)

ranger = even_range(0,10)
print(next(ranger))
print(next(ranger))



def accumulator ():
    total = 0
    while True:
        value = yield total
        print('Got: {}'.format(value))

        if not value: break
        total += value

generator = accumulator()

next(generator)
print('Accumulated: {}'.format(generator.send(1)))
print('Accumulated: {}'.format(generator.send(2)))
print('Accumulated: {}'.format(generator.send(3)))
print('Accumulated: {}'.format(generator.send(4)))
