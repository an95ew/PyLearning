import requests
import sys

url = "https://github.com/not_found"

try:
    response = requests.get(url, timeout=30)
    response.raise_for_status()
except requests.Timeout:
    print ("ERROR TIMEOUT, url:", url)
except requests.HTTPError as err:
    code = err.response.status_code
    print("ERORR URL: {0}, code: {1}".format(url, code))
except requests.RequestException:
    print("ERROR URL DOWNLOADING: ", url)
    
else:
    print(response.content)
    
