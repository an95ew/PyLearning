#####################Объявление байтовой строки##################################
b_string = b"Stop"
print(type(b_string))

for letter in b_string:
    print(letter)

#####################Кодировка  байтовой строки##################################
b_string = "приветы"
b_string = b_string.encode(encoding="utf-8")
print(b_string)
print(b_string.decode())
