from functools import wraps as savist

def logger (file_name):
    def decorator (func):
        @savist(func)
        def wrapp(*args, **kwargs):
            result = func(*args, **kwargs)
            with open (file_name, 'w') as f:
                f.write(str(result))
            return result
        return wrapp
    return decorator

@logger('test_log.txt')
def summator(numbs_list):
    return sum(numbs_list)

print('summator: {}'.format(summator([1,2,3,4,5,6,7,8])))
with open ('test_log.txt', 'r') as f:
    print('test_log.txt: {}'.format(f.read()))

print(summator.__name__)
